
class StructureDef:
    def __init__(self, struct: "", path: "", filename: "", line_start: 0):
        self._struct = struct
        self._path = path
        self._filename = filename
        self._line_start = line_start

    def get_struct(self):
        return self._struct

    def get_path(self):
        return self._path

    def get_filename(self):
        return self._filename

    def get_line_start(self):
        return self._line_start


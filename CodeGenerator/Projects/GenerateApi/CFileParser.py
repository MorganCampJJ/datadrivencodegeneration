
from Utility import FileUtility


BRACKET_OPEN = "{"
BRACKET_CLOSE = "}"
COMMENT_OPEN = "/*"
COMMENT_CLOSE = "*/"
QUOTE_START = "\""
QUOTE_END = "\""
CODE_GEN_START = "CODE_GEN_START"
CODE_GEN_IGNORE = "CODE_GEN_IGNORE"
CODE_GEN_EXPAND = "CODE_GEN_EXPAND"
LINE_COMMENT = "//"
STRUCT_START_1 = "typedef struct"
STRUCT_START_2 = "struct"

searching_for = CODE_GEN_START


def get_all_structs(path):
    if FileUtility.is_dir(path) is True:
        print("is dir")
    elif FileUtility.is_file(path) is True:
        print("is file")
    else:
        print("Path specified is neither a directory nor a file.")


def parse_line(line, substr):
    print(line)
    print(substr)
    # pre-process entire file before calling this function:
    #   - remove all code in block comments
    #   - remove all code with CODE_GEN_IGNORE in line
    #   - remove all code following //
    #   - replace double-spaces with single spaces until no double spaces are found
    #   - replace tabs with single-spaces

    # If open bracket previously found and CODE_GEN_START not found yet, search for close bracket.
    # If comment open previously found, search for comment close.
    #
    # If none of the above found, pre-process line:
    #   - Remove double spaces
    #   - Replace tabs with single spaces
    #   - Remove
    #   - Find Code Gen Start if not already started
    #   - If code gen started, find struct if not found





import sys
import CFileParser

sys.path.append("../../")

from Utility import FileUtility


# replacement strings
WINDOWS_LINE_ENDING = b'\r\n'
UNIX_LINE_ENDING = b'\n'


def main():
    if len(sys.argv) == 2:
        print('Argument List:', str(sys.argv))
        print('Number of arguments:', len(sys.argv), 'arguments.')
    else:
        print("Expected one argument: Input dir to parse")
        if FileUtility.is_dir("../../Input"):
            print("Continuing with local example at CodeGenerator/Input")
            CFileParser.get_all_structs("../../Input")


if __name__ == '__main__':
    main()

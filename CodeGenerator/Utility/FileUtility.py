import os, shutil


def remove_dir_contents(folder):
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            print(e)


def file_exists(file_and_path_str):
    return os.path.isfile(file_and_path_str)


def rename_file(source, destination):
    os.rename(source, destination)


def delete_file(src):
    os.remove(src)


def copy_file(src, dst):
    shutil.copyfile(src, dst)


def copy_dir(src, dst):
    shutil.copytree(src, dst)


def remove_dir(src):
    if os.path.isdir(src) is True:
        shutil.rmtree(src)


def make_dir_if_not_exists(dst):
    if os.path.isdir(dst) is False:
        os.mkdir(dst)


def is_dir(f):
    return os.path.isdir(f)


def is_file(f):
    return os.path.isfile(f)


def get_all_files_in_path(path):
    file_path_list = [path]
    iterator = 0
    # If it's a directory, remove it and add all subdirectories / files to the end.
    # otherwise, move to next.
    while iterator < len(file_path_list):
        if is_dir(file_path_list[iterator]):
            files_in_current = os.listdir(file_path_list[iterator])
            for f in files_in_current:
                f = file_path_list[iterator] + "/" + f
                file_path_list.append(f)

            file_path_list.pop(iterator)
        else:
            iterator = iterator + 1

    for f in file_path_list:
        print(f)
    return file_path_list


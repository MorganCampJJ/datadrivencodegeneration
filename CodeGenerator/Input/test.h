
typedef struct {
   uint8_t value1;
   uint32_t val2; // CODE_GEN_IGNORE
} OTHER_STRUCT

typedef enum {
   ENUM1,
   ENUM2,
   ENUM_MAX,
} ENUM_NAME

typedef union {
   uint16_t val1;
   uint32_t val2; // CODE_GEN_IGNORE
} UNION_NAME

/// CODE_GEN_START 
typedef struct {
  uint8_t value1;
  OTHER_STRUCT value2; //CODE_GEN_USE_SUB
  OTHER_STRUCT value3;
  const uint8_t value4;
  uint8_t* value5;
  uint8_t value6[10];
  uint8_t value7[ENUM_MAX];
  uint8_t value8; // CODE_GEN_IGNORE
  UNION_NAME value9; //CODE_GEN_USE_SUB
  UNION_NAME value10;
} STRUCT_NAME;
